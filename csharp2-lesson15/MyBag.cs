﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace csharp_lesson14
{
    class MyBag
    {
        public List<object> Items { get; set; }

        private Stopwatch _stopWatch = new Stopwatch();
        public string one;
        public string two;
        public string three;
        public MyBag(string x)
        {
            if (one == null)
            {
                one = x;
            }
            else if (one != null && two == null)
            {
                two = x;
            }
            else
            {
                three = x;
            }
        }

        ~MyBag()
        {

        }
    }
}
