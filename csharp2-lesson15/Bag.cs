using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Destructors
{
    class Bag
    {
        public List<object> Items { get; set; }
        
        private Stopwatch _stopWatch = new Stopwatch();
        
        public Bag()
        {
            _stopWatch.Start();
            Console.WriteLine("Bag constructor, start stop watch");
            Items = new List<object>();
        }

        public void Add(string x)
        {
            Items.Add(x);
        }

        public void Add(int x)
        {
            Items.Add(x);
        }

        public static Bag operator -(Bag a, Bag b)
        {
            foreach (var x in a.Items)
            {
                if (a.Items == b.Items)
                {
                    a.Items.Remove(x);
                    b.Items.Remove(x);
                }
            }
            return b;
        }
        
        ~Bag()
        {
            _stopWatch.Stop();
            Console.WriteLine("Bag destructor, stop watch stopped, object existed for " +
                _stopWatch.ElapsedMilliseconds / 1000 + " seconds");
        }
    }
}

