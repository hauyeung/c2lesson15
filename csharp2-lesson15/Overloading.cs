using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Overloading
{
    public partial class Overloading : Form
    {
        private Button exitButton;
        private ListBox outputListBox;
        private Destructors.Bag _myBag = new Destructors.Bag();
        
        public Overloading()
        {
            InitializeComponent();
        }



        private void InitializeComponent()
        {
            this.exitButton = new System.Windows.Forms.Button();
            this.outputListBox = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // exitButton
            // 
            this.exitButton.Location = new System.Drawing.Point(218, 450);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(75, 23);
            this.exitButton.TabIndex = 0;
            this.exitButton.Text = "Exit";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click_1);
            // 
            // outputListBox
            // 
            this.outputListBox.FormattingEnabled = true;
            this.outputListBox.Location = new System.Drawing.Point(23, 13);
            this.outputListBox.Name = "outputListBox";
            this.outputListBox.Size = new System.Drawing.Size(486, 420);
            this.outputListBox.TabIndex = 1;

            // 
            // Overloading
            // 
            this.ClientSize = new System.Drawing.Size(541, 498);
            this.Controls.Add(this.outputListBox);
            this.Controls.Add(this.exitButton);
            this.Name = "Overloading";
            this.Text = "Overloading";
            this.Load += new System.EventHandler(this.Overloading_Load);
            this.ResumeLayout(false);

        }



        private void exitButton_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Overloading_Load(object sender, EventArgs e)
        {
            // Declare some content data.
            string lastName = "Lastname";
            int age = 21;

            // Add the content data to our permanent bag.
            _myBag.Add(age);
            _myBag.Add(lastName);

            // Create a temporary bag, and add string and integer literal contents.
            Destructors.Bag _temporaryBag = new Destructors.Bag();
            _temporaryBag.Add("Second Bag String");
            _temporaryBag.Add(100);

            // Add the contents of our temporary bag to our permanent bag.
            _myBag.Items.Add(_temporaryBag.Items);
            Destructors.Bag c = _myBag - _temporaryBag;
            foreach (object x in c.Items)
            {
                outputListBox.Items.Add(x.ToString());
            }

            // Output the contents of our permanent bag to the ListBox.
            foreach (object contents in _myBag.Items)
            {
                outputListBox.Items.Add(contents.ToString());
            }
        }
    }
}

